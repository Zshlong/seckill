package com.zshlong.seckill.service;


import com.zshlong.seckill.config.UserArgumentResolver;
import com.zshlong.seckill.exception.GlobalException;
import com.zshlong.seckill.mapper.SeckillUserMapper;
import com.zshlong.seckill.pojo.SeckillUser;
import com.zshlong.seckill.prefix.SeckillUserKey;
import com.zshlong.seckill.util.MD5Util;
import com.zshlong.seckill.util.RedisUtil;
import com.zshlong.seckill.util.UUIDUtil;
import com.zshlong.seckill.vo.CodeMsg;
import com.zshlong.seckill.vo.LoginVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Service
public class SeckillUserService {

	public static final String COOKI_NAME_TOKEN = "token";
	
	@Autowired
	private SeckillUserMapper seckillUserMapper;

	@Autowired
	private RedisUtil redisUtil;


	public SeckillUser getById(long id) {
		//取缓存
		SeckillUser user =(SeckillUser) redisUtil.get(SeckillUserKey.getById, ""+id);
		if(user != null) {
			return user;
		}
		//取数据库
		user = seckillUserMapper.getById(id);

		//将数据库user添加缓存
		if(user != null) {
			redisUtil.set(SeckillUserKey.getById, ""+id, user);
		}
		return user;
	}

	// http://blog.csdn.net/tTU1EvLDeLFq5btqiK/article/details/78693323
	public boolean updatePassword(String token, long id, String formPass) {
		//取user
		SeckillUser user = getById(id);
		if(user == null) {
			throw new GlobalException(CodeMsg.MOBILE_NOT_EXIST);
		}
		//更新数据库
		SeckillUser toBeUpdate = new SeckillUser();
		toBeUpdate.setId(id);
		toBeUpdate.setPassword(MD5Util.formPassToDBPass(formPass, user.getSalt()));
		seckillUserMapper.update(toBeUpdate);
		//处理缓存
		redisUtil.delete(SeckillUserKey.getById, ""+id);
		user.setPassword(toBeUpdate.getPassword());
		redisUtil.set(SeckillUserKey.token, token, user);
		return true;
	}


	public SeckillUser getByToken(HttpServletResponse response, String token) {
		if(StringUtils.isEmpty(token)) {
			return null;
		}
		SeckillUser user =(SeckillUser) redisUtil.get(SeckillUserKey.token, token);
		//延长有效期
		if(user != null) {
			addCookie(response, token, user);
		}
		return user;
	}

	public String login(HttpServletResponse response, LoginVo loginVo) {
		if(loginVo == null) {
			throw new GlobalException(CodeMsg.SERVER_ERROR);
		}
		String mobile = loginVo.getMobile();
		String formPass = loginVo.getPassword();
		//判断手机号是否存在
		SeckillUser user = getById(Long.parseLong(mobile));
		if(user == null) {
			throw new GlobalException(CodeMsg.MOBILE_NOT_EXIST);
		}
		//验证密码
		String dbPass = user.getPassword();
		String saltDB = user.getSalt();
		String calcPass = MD5Util.formPassToDBPass(formPass, saltDB);
//		System.out.println("数据库密码："+dbPass);
//		System.out.println("最终密码:"+calcPass);
		if(!calcPass.equals(dbPass)) {
			throw new GlobalException(CodeMsg.PASSWORD_ERROR);
		}
		//生成cookie
		String token = UUIDUtil.uuid();
		addCookie(response, token, user);
		return token;
	}

	private void addCookie(HttpServletResponse response, String token, SeckillUser user) {
		//token写入缓存
		redisUtil.set(SeckillUserKey.token, token, user);
		//创建cookies
		Cookie cookie = new Cookie(COOKI_NAME_TOKEN, token);
		cookie.setMaxAge((int)SeckillUserKey.token.expireSeconds());
		cookie.setPath("/");
		response.addCookie(cookie);
	}
}
