package com.zshlong.seckill.service;

import com.zshlong.seckill.pojo.OrderInfo;
import com.zshlong.seckill.pojo.SeckillOrder;
import com.zshlong.seckill.pojo.SeckillUser;
import com.zshlong.seckill.prefix.SeckillKey;
import com.zshlong.seckill.util.MD5Util;
import com.zshlong.seckill.util.RedisUtil;
import com.zshlong.seckill.util.UUIDUtil;
import com.zshlong.seckill.vo.GoodsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Random;

@Service
public class SeckillService {
	
	@Autowired
	GoodsService goodsService;
	
	@Autowired
	OrderService orderService;

	@Autowired
	RedisUtil redisUtil;

	@Transactional
	public OrderInfo seckill(SeckillUser user, GoodsVo goods) {
		//减库存 下订单 写入秒杀订单
		boolean successs = goodsService.reduceStock(goods);
		if(successs){
			//order_info seckill_order
			return orderService.createOrder(user, goods);
		}else {
			setGoodsOver(goods.getId());
			return null;
		}
	}

	public long getSeckillResult(Long userId, long goodsId) {
		SeckillOrder order = orderService.getSeckillOrderByUserIdGoodsId(userId, goodsId);
		if(order != null) {//秒杀成功
			return order.getOrderId();
		}else {
			boolean isOver = getGoodsOver(goodsId);
			if(isOver) {
				return -1;
			}else {
				return 0;
			}
		}
	}

	private void setGoodsOver(Long goodsId) {
		redisUtil.set(SeckillKey.isGoodsOver, ""+goodsId, true);
	}

	private boolean getGoodsOver(long goodsId) {
		return redisUtil.exists(SeckillKey.isGoodsOver, ""+goodsId);
	}

	public boolean checkPath(SeckillUser user, long goodsId, String path) {
		if(user == null || path == null) {
			return false;
		}
		String pathOld = (String) redisUtil.get(SeckillKey.getMiaoshaPath, ""+user.getId() + "_"+ goodsId);
		return path.equals(pathOld);
	}


	/** 秒杀路径MD5加盐加密存入Redis中缓存 */
	public String createSeckillPath(SeckillUser user, long goodsId) {
		if(user == null || goodsId <=0) {
			return null;
		}
		String path = MD5Util.md5(UUIDUtil.uuid()+"123456");
		redisUtil.set(SeckillKey.getMiaoshaPath, ""+user.getId() + "_"+ goodsId, path);
		return path;
	}

	public BufferedImage createVerifyCode(SeckillUser user, long goodsId) {
		if(user == null || goodsId <=0) {
			return null;
		}
		int width = 80;
		int height = 32;
		//create the image
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics g = image.getGraphics();
		// set the background color
		g.setColor(new Color(0xDCDCDC));
		g.fillRect(0, 0, width, height);
		// draw the border
		g.setColor(Color.black);
		g.drawRect(0, 0, width - 1, height - 1);
		// create a random instance to generate the codes
		Random rdm = new Random();
		// make some confusion
		for (int i = 0; i < 50; i++) {
			int x = rdm.nextInt(width);
			int y = rdm.nextInt(height);
			g.drawOval(x, y, 0, 0);
		}
		// generate a random code
		String verifyCode = generateVerifyCode(rdm);
		g.setColor(new Color(0, 100, 0));
		g.setFont(new Font("Candara", Font.BOLD, 24));
		g.drawString(verifyCode, 8, 24);
		g.dispose();
		//把验证码存到redis中
		int rnd = calc(verifyCode);
		redisUtil.set(SeckillKey.getMiaoshaVerifyCode, user.getId()+","+goodsId, rnd);
		//输出图片
		return image;
	}

	public boolean checkVerifyCode(SeckillUser user, long goodsId, int verifyCode) {
		if(user == null || goodsId <=0) {
			return false;
		}
		Integer codeOld =(Integer) redisUtil.get(SeckillKey.getMiaoshaVerifyCode, user.getId()+","+goodsId);
		if(codeOld == null || codeOld - verifyCode != 0 ) {
			return false;
		}
		redisUtil.delete(SeckillKey.getMiaoshaVerifyCode, user.getId()+","+goodsId);
		return true;
	}

	private static int calc(String exp) {
		try {
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			return (Integer)engine.eval(exp);
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	private static char[] ops = new char[] {'+', '-', '*'};
	/**
	 * + - *
	 * */
	private String generateVerifyCode(Random rdm) {
		int num1 = rdm.nextInt(10);
		int num2 = rdm.nextInt(10);
		int num3 = rdm.nextInt(10);
		char op1 = ops[rdm.nextInt(3)];
		char op2 = ops[rdm.nextInt(3)];
		String exp = ""+ num1 + op1 + num2 + op2 + num3;
		return exp;
	}

	public void reset(List<GoodsVo> goodsList) {
		goodsService.resetStock(goodsList);
		orderService.deleteOrders();
	}
}