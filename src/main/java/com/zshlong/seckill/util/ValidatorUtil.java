package com.zshlong.seckill.util;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * 参数校验工具类
 * */
public class ValidatorUtil {
	//正则表达式
	private static final Pattern mobile_pattern = Pattern.compile("1\\d{10}");

	//判断是否符合手机号码格式
	public static boolean isMobile(String src) {
		if(StringUtils.isEmpty(src)) {
			return false;
		}
		Matcher m = mobile_pattern.matcher(src);
		return m.matches();
	}
	
//	public static void main(String[] args) {
//			System.out.println(isMobile("18912341234"));
//			System.out.println(isMobile("1891234123"));
//	}
}
