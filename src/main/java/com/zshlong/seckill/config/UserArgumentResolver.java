package com.zshlong.seckill.config;

import com.zshlong.seckill.access.UserContext;
import com.zshlong.seckill.pojo.SeckillUser;
import com.zshlong.seckill.service.SeckillUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

@Service
public class UserArgumentResolver implements HandlerMethodArgumentResolver {

	@Autowired
    SeckillUserService userService;

	// 返回true，方可进入resolveArgument做参数处理
	public boolean supportsParameter(MethodParameter parameter) {
//		Class<?> clazz = parameter.getParameterType();
//		return clazz==SeckillUser.class;
		return parameter.getParameterType().isAssignableFrom(SeckillUser.class);
	}

	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
								  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		return UserContext.getUser();
	}
}
