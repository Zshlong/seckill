package com.zshlong.seckill.controller;

import com.zshlong.seckill.pojo.OrderInfo;
import com.zshlong.seckill.pojo.SeckillUser;
import com.zshlong.seckill.service.GoodsService;
import com.zshlong.seckill.service.OrderService;
import com.zshlong.seckill.service.SeckillUserService;
import com.zshlong.seckill.util.RedisUtil;
import com.zshlong.seckill.vo.CodeMsg;
import com.zshlong.seckill.vo.GoodsVo;
import com.zshlong.seckill.vo.OrderDetailVo;
import com.zshlong.seckill.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/order")
public class OrderController {

	@Autowired
	SeckillUserService userService;
	
	@Autowired
	RedisUtil redisUtil;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	GoodsService goodsService;
	
    @RequestMapping("/detail")
    @ResponseBody
    public Result<OrderDetailVo> info(SeckillUser user, @RequestParam("orderId") long orderId) {
    	if(user == null) {
    		return Result.error(CodeMsg.SESSION_ERROR);
    	}
    	OrderInfo order = orderService.getOrderById(orderId);
    	if(order == null) {
    		return Result.error(CodeMsg.ORDER_NOT_EXIST);
    	}

    	long goodsId = order.getGoodsId();
    	GoodsVo goods = goodsService.getGoodsVoByGoodsId(goodsId);
    	OrderDetailVo vo = new OrderDetailVo();
    	vo.setOrder(order);
    	vo.setGoods(goods);
    	return Result.success(vo);
    }
}