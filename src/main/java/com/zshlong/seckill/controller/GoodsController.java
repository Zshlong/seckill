package com.zshlong.seckill.controller;


import com.zshlong.seckill.pojo.SeckillUser;
import com.zshlong.seckill.prefix.GoodsKey;
import com.zshlong.seckill.service.GoodsService;
import com.zshlong.seckill.util.RedisUtil;
import com.zshlong.seckill.vo.GoodsDetailVo;
import com.zshlong.seckill.vo.GoodsVo;
import com.zshlong.seckill.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.context.IWebContext;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/goods")
@Slf4j
public class GoodsController {

    @Autowired
    GoodsService goodsService;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    ThymeleafViewResolver thymeleafViewResolver;

    @Autowired
    ApplicationContext applicationContext;


    // https://blog.csdn.net/qq_36505948/article/details/82620908
    // 秒杀商品列表页 页面缓存
    @RequestMapping(value = "toList",produces ="text/html")
    @ResponseBody
    public String list(HttpServletRequest request, HttpServletResponse response, Model model, SeckillUser user){
        model.addAttribute("user", user);

        // 取缓存
        String html = (String) redisUtil.get(GoodsKey.getGoodsList, "");
        if(!StringUtils.isEmpty(html)) {
            return html;
        }

        // 查询商品列表
        List<GoodsVo> goodsList = goodsService.listGoodsVo();
        model.addAttribute("goodsList",goodsList);
//        return "goods_list";

        IWebContext webContext = new WebContext(request,response,
                request.getServletContext(),request.getLocale(), model.asMap());
        // 手动渲染
        html = thymeleafViewResolver.getTemplateEngine().process("goods_list", webContext);
        if(!StringUtils.isEmpty(html)) {// 页面缓存
            redisUtil.set(GoodsKey.getGoodsList, "", html);
        }
        return html;
    }


    // 秒杀商品详情页 页面部分静态化
    @RequestMapping("/detail/{goodsId}")
    @ResponseBody
    public Result<GoodsDetailVo> detail(SeckillUser user, @PathVariable("goodsId")long goodsId) {
        GoodsVo goods = goodsService.getGoodsVoByGoodsId(goodsId);

        long startAt = goods.getStartDate().getTime();
        long endAt = goods.getEndDate().getTime();
        long now = System.currentTimeMillis();

        int seckillStatus = 0;//秒杀状态
        int remainSeconds = 0;//剩余时间
        if(now < startAt ) {//秒杀还没开始，倒计时
            seckillStatus = 0;
            remainSeconds = (int)((startAt - now )/1000);
        }else  if(now > endAt){//秒杀已经结束
            seckillStatus = 2;
            remainSeconds = -1;
        }else {//秒杀进行中
            seckillStatus = 1;
            remainSeconds = 0;
        }

        GoodsDetailVo vo = new GoodsDetailVo();
        vo.setGoods(goods);
        vo.setUser(user);
        vo.setRemainSeconds(remainSeconds);
        vo.setSeckillStatus(seckillStatus);
        return Result.success(vo);
    }

}
