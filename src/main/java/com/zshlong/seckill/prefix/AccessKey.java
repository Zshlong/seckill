package com.zshlong.seckill.prefix;

public class AccessKey extends BasePrefix{

	private AccessKey( long expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}
	
	public static AccessKey withExpire(long expireSeconds) {
		return new AccessKey(expireSeconds, "access");
	}
	
}
