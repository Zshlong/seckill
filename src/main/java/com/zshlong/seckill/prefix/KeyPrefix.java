package com.zshlong.seckill.prefix;

public interface KeyPrefix {
		
	public long expireSeconds();
	
	public String getPrefix();
	
}
