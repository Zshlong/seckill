package com.zshlong.seckill.prefix;

public class SeckillKey extends BasePrefix{

	private SeckillKey(long expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}


	public static SeckillKey isGoodsOver = new SeckillKey(0, "go");
	public static SeckillKey getMiaoshaPath = new SeckillKey(60, "mp");
	public static SeckillKey getMiaoshaVerifyCode = new SeckillKey(300, "vc");

}
