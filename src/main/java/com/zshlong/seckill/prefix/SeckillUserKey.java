package com.zshlong.seckill.prefix;

public class SeckillUserKey extends BasePrefix{

	public static final long TOKEN_EXPIRE = 3600*24 * 2;
	private SeckillUserKey(long expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}


	public static SeckillUserKey token = new SeckillUserKey(TOKEN_EXPIRE, "tk");
	public static SeckillUserKey getById = new SeckillUserKey(0, "id");
}
