package com.zshlong.seckill.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderInfo {

	private Long id;
	private Long userId;
	private String userName;
	private Long goodsId;
	private String deliveryAddress;
	private String goodsName;
	private Integer goodsCount;
	private BigDecimal goodsPrice;
	private Integer orderChannel;
	private Integer status;
	private Date createDate;
	private Date payDate;

}