package com.zshlong.seckill.vo;

import com.zshlong.seckill.pojo.OrderInfo;
import lombok.Data;

@Data
public class OrderDetailVo {

	private GoodsVo goods;
	private OrderInfo order;

}