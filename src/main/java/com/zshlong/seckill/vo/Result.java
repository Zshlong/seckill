package com.zshlong.seckill.vo;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {
	
	private int code;
	private String msg;
	private T data;
	
	/** 成功时候的调用 */
	public static  <T> Result<T> success(T data){
		return new Result<T>(data);
	}
	
	/** 失败时候的调用 */
	public static  <T> Result<T> error(CodeMsg codeMsg){
		return new Result<T>(codeMsg);
	}


	private Result(T data) {//成功
		this.code = CodeMsg.SUCCESS.getCode();
		this.msg = CodeMsg.SUCCESS.getMsg();
		this.data = data;
	}
	private Result(CodeMsg codeMsg) {
		if(codeMsg != null) {
			this.code = codeMsg.getCode();
			this.msg = codeMsg.getMsg();
		}
	}

}
