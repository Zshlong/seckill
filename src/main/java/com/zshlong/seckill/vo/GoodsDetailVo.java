package com.zshlong.seckill.vo;

import com.zshlong.seckill.pojo.SeckillUser;
import lombok.Data;

@Data
public class GoodsDetailVo {

	private int seckillStatus = 0;
	private int remainSeconds = 0;
	private GoodsVo goods ;
	private SeckillUser user;

}