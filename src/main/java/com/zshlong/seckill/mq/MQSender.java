package com.zshlong.seckill.mq;


import com.zshlong.seckill.config.RabbitMQConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MQSender {

    private static Logger log = LoggerFactory.getLogger(MQSender.class);

    @Autowired
    RabbitTemplate rabbitTemplate;

    public void sendSeckillMessage(SeckillMessage msg) {
        log.info("send message:"+msg);
        rabbitTemplate.convertAndSend(RabbitMQConfig.SECKILL_EX,"seckill", msg);
    }
}
