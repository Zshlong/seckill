package com.zshlong.seckill.mq;

import com.zshlong.seckill.pojo.SeckillUser;
import lombok.Data;

@Data
public class SeckillMessage {

    private SeckillUser user;
    private long goodsId;

}
