package com.zshlong.seckill.mapper;

import com.zshlong.seckill.pojo.SeckillGoods;
import com.zshlong.seckill.vo.GoodsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface GoodsMapper {

	@Select("SELECT g.*,mg.seckill_price,mg.stock_count, mg.start_date, mg.end_date FROM seckill.miaosha_goods mg LEFT JOIN seckill.goods g ON mg.goods_id = g.id")
	List<GoodsVo> listGoodsVo();

	@Select("SELECT g.*,mg.seckill_price,mg.stock_count, mg.start_date, mg.end_date FROM seckill.miaosha_goods mg LEFT JOIN seckill.goods g ON mg.goods_id = g.id where g.id = #{goodsId}")
	GoodsVo getGoodsVoByGoodsId(@Param("goodsId")long goodsId);

	@Update("update seckill.miaosha_goods set stock_count = stock_count - 1 where goods_id = #{goodsId} and stock_count > 0")
	int reduceStock(SeckillGoods g);

	@Update("update seckill.miaosha_goods set stock_count = #{stockCount} where goods_id = #{goodsId}")
	int resetStock(SeckillGoods g);
	
}