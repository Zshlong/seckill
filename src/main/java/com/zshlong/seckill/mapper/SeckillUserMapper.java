package com.zshlong.seckill.mapper;


import com.zshlong.seckill.pojo.SeckillUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface SeckillUserMapper {
	
	@Select("select * from seckill.miaosha_user where id = #{id}")
    SeckillUser getById(@Param("id")long id);

	@Update("update seckill.miaosha_user set password = #{password} where id = #{id}")
	void update(SeckillUser toBeUpdate);
}
